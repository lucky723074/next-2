import { combineReducers } from "redux";
import {ourWidgetReducer} from "./components/Widget/Widget.reducers"
export const rootReducer = combineReducers({
	ourWidget : ourWidgetReducer,
});