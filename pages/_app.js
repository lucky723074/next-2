import '../styles/globals.css'
import { Provider } from 'react-redux'
import Head from 'next/head'
import Layout from '../components/Layout/Layout'
import { store } from '../store'

function MyApp({ Component, pageProps }) {
	return (
			<Provider store={store}>
				<Layout>
					<Head>
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					</Head>
					<Component {...pageProps} />
				</Layout>
			</Provider>
	)
}

export default MyApp
