import React from 'react'
import Footer from './components/Footer/Footer'
import Header from './components/Header/Header'
import MenuBar from './components/MenuBar/MenuBar'

export default function Layout({children}) {
	return (
		<>
			<div id="wrapper">
				<MenuBar />
				<div id="content-wrapper" className="d-flex flex-column">
					<div id="content">
						<Header />
						{children}
					</div>
					<Footer />
				</div>
			</div>
			<a className="scroll-to-top rounded" href="#page-top">
        <i className="fas fa-angle-up"></i>
	    </a>
		</>
	)
}
