import React from 'react'
import Cards from './components/Cards'

export default function Widget( {widgets = []}) {
	return (
		<>
			<div className="row">
				{widgets.map((data,index)=><Cards key={index} {...data} />)}
			</div>
		</>
	)
}
