import React from 'react'
import { useSelector } from 'react-redux'
import Widget from './Widget'

export default function WidgetContainer() {
	const widgetData = useSelector(state=>state.ourWidget)
	return <Widget widgets={widgetData}/>
	
}
