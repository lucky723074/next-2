const initialState = [
	{
		name: "Earnings (Monthly)",
		price: "$100",
		colorClass: "border-left-primary",
		icon: "fas fa-calendar"
	},
	{
		name: "Binary Income",
		price: "$200",
		colorClass: "border-left-info",
		icon: "fas fa-list"
	},
	{
		name: "Direct Income",
		price: "$300",
		colorClass: "border-left-danger",
		icon: "fas fa-user"
	},
	{
		name: "Spill Income",
		price: "$400",
		colorClass: "border-left-success",
		icon: "fas fa-folder"
	}
]
export const ourWidgetReducer = (state = initialState, action) => {
	switch (action.type) {
		default:
			return state;
	}
};